package labs.lab4;

public class Potion {
	private String name;
	private double strength;
	
	// default con
	public Potion (){
		this.name = "";
		this.strength = 0; 
	}
	// non default const
	public Potion(String name, double strength) {
	this.name= name;
	this.strength = strength;
	}
	// name access
	public String getName() {
		return this.name;
	}
	// strength access
	public double getStrength() {
		return this.strength;
	}
	
	// mutator name
	public void setName(String name) {
		this.name= name;
		
	}
	// mutator strength
	public void setStrength(double strength) {
		this.strength = strength;
	}
	public String toString() {
		String txt = "Potion1= name:" + this.name+"  strength:"+this.strength;
		return txt;
	}
	
	public boolean checkStrength(){
		if(this.strength >= 0 && this.strength <= 10) {
			return true;
		}else {
			return false;
			  }
		}
	
	public boolean equals(Object Potion ) { 
		if (Potion == null) {
			return false;
		}else {
			if(this == Potion) {
				return true;
			}else {
				return false;
			}
		}
	}
	}
		

		
	