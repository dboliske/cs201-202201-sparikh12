package labs.lab4;

public class PhoneNumber {
	private String countryCode;
	private String areaCode;
	private String number;
	// default const
	public PhoneNumber() {
		this.areaCode = "000";
		this.countryCode = "00";
		this.number = "0000000";
	}
	// non default const
	public PhoneNumber( String countryCode, String areaCode, String number) {
		this.countryCode = countryCode;
		this.areaCode = areaCode;
		this.number = number;
	}
	// countryCode access
	public String getCountryCode() {
		return this.countryCode;
	}
	// areaCode access
	public String getAreaCode() {
		return this.areaCode;
	}
	// number access
	public String getNumber() {
		return this.number;
	}
	
	// mutator countryCode
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}
	// mutator areaCode
	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}
	// mutator number
	public void setNumber(String number) {
		this.number = number;
	}
	// string converter
	public String toString() {
		return ("+"+this.countryCode+" ("+this.areaCode+") "+this.number);
	}
	// requirement check 
	public boolean checkAreaCode() {
		if (this.areaCode.length() == 3) {
			return true;
		}else {
			return false;
		}
	}
	// requirement check 
	public boolean checkNumber() {
		if(this.number.length() == 7) {
			return true;
		}else {
			return false;
		}
	}
	// compares this instances to other GeoLocation
	public boolean equals(Object PhoneNumber ) {
		if(PhoneNumber  == null) {
			return false;
		}else {
			if(this == PhoneNumber) {
				return true;
			}else {
				return false;
			}
		}
	}

}
