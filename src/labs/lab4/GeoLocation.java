package labs.lab4;

public class GeoLocation {
	private double lat;
	private double lng;
	
	// default const
	public GeoLocation() {
		this.lat = this.lng = 0;
	}
	
	// non default const
	public GeoLocation(double lat, double lng) {
		this.lat = lat;
		this.lng = lng;
	}
	
	// lat access
	public double getLat() {
		return this.lat;
	}
	
	// lng access
	public double getLng() {
		return this.lng;
	}
	
	// mutator lat
	public void setLat(double lat) {
		this.lat = lat;
	}
	
	// mutator lng
	public void setLng(double lng) {
		this.lng = lng;
	}
	// string converter
	public String toString() {
		String txt = "("+this.lat+", "+this.lng+")";
		return txt;
	}
	// requirement check
	public boolean checkLat() {
		if(this.lat > -90 && this.lat < 90) {
			return true;
		}else {
			return false;
		}
	}
	// requirement check 
	public boolean checkLng() {
		if(this.lng > -180 && this.lng < 180) {
			return true;
		}else {
			return false;
		}
	}
	// compares this instances to other GeoLocation
	public boolean equals(Object GeoLocation) {
		if(GeoLocation == null) {
			return false;
		}else {
			if(this == GeoLocation) {
				return true;
			}else {
				return false;
			}
		}
		
	}
	
	
	
}


