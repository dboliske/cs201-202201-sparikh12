package labs.lab4;

public class GeoLocation_test {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		GeoLocation GeoLocation1 = new GeoLocation();
		GeoLocation GeoLocation2 = new GeoLocation(45.0,90.0);
		
		//print
		System.out.println("Program Name:GeoLocation_Test");
		System.out.println("Name:SWAR PARIKH");
		System.out.println("Course:CS 201-02");
		System.out.println("Date:Feb 21 2022");
		System.out.println("     ");
		System.out.println("GeoLocation1 = lat:"+GeoLocation1.getLat()+" lng:"+GeoLocation1.getLng());//calls the default constructor
		System.out.println("GeoLocation2 = lat:"+GeoLocation2.getLat()+" lng:"+GeoLocation2.getLng());//calls the non-default constructor 
		
	}

}
