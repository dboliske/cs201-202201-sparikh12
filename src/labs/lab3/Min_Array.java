package labs.lab3;

public class Min_Array {
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//3.Write a program that will find the minimum value and print it to the console for the given array:
		// Array :- # Lab 3 - Arrays and String Parsing 
		// {72, 101, 108, 108, 111, 32, 101, 118, 101, 114, 121, 111, 110, 101, 33, 32, 76, 111, 111, 107, 32, 97, 116, 32, 116, 104, 101, 115, 101, 32, 99, 111, 111, 108, 32, 115, 121, 109, 98, 111, 108, 115, 58, 32, 63264, 32, 945, 32, 8747, 32, 8899, 32, 62421}
		System.out.println("Program Name:Min_Array");
		System.out.println("Name:SWAR PARIKH");
		System.out.println("Course:CS 201-02");
		System.out.println("Date:Jan 31 2022");
		System.out.println("         ");
		 int[] array = new int[]{72, 101, 108, 108, 111, 32, 101, 118, 101, 114, 121, 111, 110, 101, 33, 32, 76, 111, 111, 107, 32, 97, 116, 32, 116, 104, 101, 115, 101, 32, 99, 111, 111, 108, 32, 115, 121, 109, 98, 111, 108, 115, 58, 32, 63264, 32, 945, 32, 8747, 32, 8899, 32, 62421}; 
		 // Declaring array literal
		 int small = 0;
		 for(int i = 0;i<array.length-1;i++) {
			 small = Math.min(array[i],array[i+1]); // Finds the smallest values
		 }
		 System.out.print("Min: "+small); // min value is printed
}
}
