package labs.lab3;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class Avg_Grade {

	public static void main(String[] args ) throws IOException {
		// TODO Auto-generated method stub
		// Instruction:- 1.You have been given a file called "src/labs/lab3/grades.csv". 
		//It contains a list of students and their exam grades. 
		//Write a program that reads in the file and computes the average grade for the class and then prints it to the console.
		System.out.println("Program Name:Avg_Grade");
		System.out.println("Name:SWAR PARIKH");
		System.out.println("Course:CS 201-02");
		System.out.println("Date:Feb 07 2022");
		 File file= new File("C:/Users/Swarp/git/cs201-202201-sparikh12/src/labs/lab3/grades.csv");
		 Scanner input = new Scanner(file);
		 int sum = 0;
		 int count = 0;
		 while (input.hasNextLine()){
			 String FirstName = input.nextLine();
			 //System.out.print(FirstName);
			
			 sum += Integer.parseInt(FirstName.split(",")[1]);			 
			 //System.out.println(" ==> Sum = "+sum);
			 count ++;
		 }
		 
		 System.out.println("Summation of all the grades = "+sum);
		 System.out.println("Number of the Students = "+count);
		 System.out.println("Grade Avg = " + (sum/count));
		
	 input.close(); 
	}
	
	};


