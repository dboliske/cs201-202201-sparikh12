package labs.lab6;

public class Customer {
	private String name;
	//default con
	public Customer() {
		this.name = "";
	}
	//non default con
	public Customer(String name) {
		this.name = name;
		
	}
	//name access
	public String getName() {
		return this.name;
	}

	
}
