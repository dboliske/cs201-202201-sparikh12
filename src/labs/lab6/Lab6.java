package labs.lab6;
import java.util.*;
import java.util.Scanner;
public class Lab6 {
	
	public static Customer add_customer() {
		
		System.out.print("Enter the name of the Customer: ");
		Scanner scan = new Scanner(System.in);
		String name = scan.nextLine();
		Customer new_customer = new Customer(name);
		
		return new_customer;
	}
	public static String remove_customer(ArrayList<Customer> cust) {
		Customer customer = cust.get(0);
		cust.remove(0);
		return customer.getName();
	}
	
	public static void main(String[] args) {
		 Scanner scan = new Scanner(System.in);
		// TODO Auto-generated method stub
		ArrayList<Customer> cust = new ArrayList<Customer>();
		System.out.println("Program Name:Deli (Lab6)");
		System.out.println("Name:SWAR PARIKH");
		System.out.println("Course:CS 201-02");
		System.out.println("Date:March 21 2022");
		boolean status = true;
		
		while(status) {
			System.out.println("1.Add customer to Queue");
			System.out.println("2.Hep customer");
			System.out.println("3.Exit");
			String pos = scan.nextLine();
			switch(pos) {
				case "1":	

				cust.add(add_customer());
				System.out.println("Position: "+cust.size());
				break;
				case "2": 
				System.out.println("Helped customer: "+remove_customer(cust));
				break;
				case "3":
				System.out.println("Exit");
				status = false;
				
				break;
			}
		}
		
		
	}

}
