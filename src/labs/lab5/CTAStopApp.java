package labs.lab5;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class CTAStopApp {

	public static void main(String[] args) throws FileNotFoundException {
		// TODO Auto-generated method stub
		System.out.println("Program Name:CTAStopApp");
		System.out.println("Name:SWAR PARIKH");
		System.out.println("Course:CS 201-02");
		System.out.println("Date:March 07 2022");
		System.out.println("     ");

		String file = ("C:\\Users\\Swarp\\git\\cs201-202201-sparikh12\\src\\labs\\lab5\\CTAStops.csv");
		CTAStation[] stations = readFile(file);


		Scanner userInput  = new Scanner(System.in);
		
		


		while (true){
			System.out.print("Enter the option: \n [1] Display Station \n [2] Display Station with/without Wheelchair access \n [3] Display Nearest Station \n [4] Exit\n");
			String input= userInput.nextLine();
		
			if(input.equals("1")) {
				for(int i = 0;i<stations.length;i++) {
					System.out.println("Station Name: "+stations[i].getName());
				}

			}else if (input.equals("2")) {
				System.out.println("Select Y for Wheelchair -> True and N -> False:");
				String input2= userInput.nextLine();
				if(input2.equalsIgnoreCase("y")) {
					for(int i = 0;i<stations.length;i++) {
						if(stations[i].hasWheelchair()) { // has wheel chair access
							System.out.println("Station Name: "+stations[i].getName()+" Wheel Chair Access:"+stations[i].hasWheelchair());
						}
					}
				}else {
					for(int i = 0;i<stations.length;i++) {
						if(!stations[i].hasWheelchair()) { // does not have wheel chair access
							System.out.println("Station Name: "+stations[i].getName()+" Wheel Chair Access:"+stations[i].hasWheelchair());
						}
					}
				}
				
				
			}else if(input.equals("3")) {
				System.out.println("Enter Lat: ");
				double lat= userInput.nextDouble();
				System.out.println("Enter Lng: ");
				double lng= userInput.nextDouble();
				
				
				
				CTAStation object1 = new CTAStation();
				object1 = stations[0];
				
			    for (int i = 0; i < stations.length; i++) {
			    	
			    	if(object1.calcDistance(lat, lng) > stations[i].calcDistance(lat, lng)) {
			    		object1 = stations[i];
			    		
			    	}
			    }
			    if(!object1.getName().equalsIgnoreCase("")) {
			    	System.out.println("Shortest Distance to given coordinates is station: "+object1.getName());
			    }else {
			    	System.out.println("Error occured");
			    }
			    
			       
			}else if(input.equals("4")) {
				break;
			}
			
		}
		
		userInput.close();
		
		
		
	}
	
	public static CTAStation[] readFile(String input) throws FileNotFoundException{ 
		//To read in the excel file and store into an array of type CTAStation[]
		
		File file = new File(input);
		Scanner df = new Scanner (file);
		System.out.println("[check 1] File opened");
		boolean wheelchair = true; // initializing variables 
		boolean open = true;
		double lat = 1.0;
		double lng = 1.0;
		String location = " ";
		String name = " ";
		int m =0; 
		CTAStation object = new CTAStation() ; //initializing the object
		
		while(df.hasNextLine()){ //Figures out how many rows are in the file
			df.nextLine();
			m++;
			
		}
		System.out.println("[check 2] Number of lines are: "+m);
		CTAStation[] list_stations = new CTAStation[m-1]; //initializing array to store all CTAStation objects
		System.out.println("[check 3] Initializing array ... ");
		df.close();
		Scanner df1 = new Scanner (file);
		
		String firstline = df1.nextLine(); // Stores the first line of the text file into a variable (Needed in order to skip the first line)

			
		int count = 0;
		while (df1.hasNext()){ // Only performs action if the file has a next line
			String txt = df1.nextLine();
			String[] station = txt.split(",");
			
			name = station[0];
			location = station[3];
			lat = Double.parseDouble(station[1]) ;
		
			lng =  Double.parseDouble(station[2]);
			wheelchair = Boolean.parseBoolean(station[4]) ;
			open = Boolean.parseBoolean(station[5]);
			
			if(!txt.equalsIgnoreCase(firstline)) {
				
				object = new CTAStation(name,location,lat,lng,wheelchair,open);
				list_stations[count] = object;
				count ++;
				
				
			}
			
			

			
			
		}
		System.out.println("[check 4] CTAStation List initiated");
		
		
		df1.close();

		System.out.println("[check 5] File closed successfully");
	
		return list_stations;  //Returns array of all stations 
	}

}
