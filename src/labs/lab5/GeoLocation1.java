package labs.lab5;

public class GeoLocation1 {

	private double lat;
	private double lng;
	
	// default const
	public GeoLocation1() {
		this.lat = this.lng = 0;
		
	}
	
	// non default const
	public GeoLocation1(double lat, double lng) {
		this.lat = lat;
		this.lng = lng;
	}
	
	// lat access
	public double getLat() {
		return this.lat;
	}
	
	// lng access
	public double getLng() {
		return this.lng;
	}
	
	// mutator lat
	public void setLat(double lat) {
		this.lat = lat;
	}
	
	// mutator lng
	public void setLng(double lng) {
		this.lng = lng;
	}
	// string converter
	public String toString() {
		String txt = "("+this.lat+", "+this.lng+")";
		return txt;
	}
	// requirement check
	
	public boolean ValidLat() {
		if(this.lat > -90 && this.lat < 90) {
			return true;
		}else {
			return false;
		}
	}
	// requirement check 
	public boolean ValidLng() {
		if(this.lng > -180 && this.lng < 180) {
			return true;
		}else {
			return false;
		}
	}
	// compares this instances to other GeoLocation
	public boolean equals(Object GeoLocation1) {
		if(GeoLocation1 == null) {
			return false;
		}else {
			if(this == GeoLocation1) {
				return true;
			}else {
				return false;
			}
		}
		
	}
	public double calcDistance(GeoLocation1 g) {// 
		double distance = Math.pow((this.lat - g.getLat() ), 2) + Math.pow((this.lng - g.getLng()), 2);
		return Math.sqrt(distance);
		// calculates distances between two points 
		
	}
	
	public double calcDistance(double lat, double lng) {
		double distance = Math.pow((this.lat - lat ), 2) + Math.pow((this.lng - lng), 2);
		return Math.sqrt(distance);
		// calculates distances between two points 
	}
	
	
	
}


