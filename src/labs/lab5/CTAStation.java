package labs.lab5;

public class CTAStation extends GeoLocation1{

	 private String name;
	 private String location;
	 private boolean wheelchair;
	 private boolean open;
	 
	 
	// default const
	public CTAStation() {
		this.setLat(0);
		this.setLng(0);
		this.name = "";
		this.location = "";
		this.wheelchair = false;
		this.open = false;
		
	}
	
	// non default const
	public CTAStation(String name, String location, double lat, double lng, boolean wheelchair, boolean open) {
		this.name = name;
		this.location = location;
		this.setLat(lat);
		this.setLng(lng);
		this.wheelchair = wheelchair;
		this.open = open;
				
	}
	// name access
	public String getName() {
			return this.name;
	}
	// name access
	public String getLocation() {
			return this.location;
	}
	public boolean hasWheelchair() {
		return this.wheelchair;
	}
	public boolean isOpen() {
		return this.open;
	}
	
	// mutator lat
	public void setName(String name) {
		this.name = name;
	}
	
	// mutator lng
	public void setLocation(String location) {
		this.location = location;
	}
	// mutator wheelchair
	public void setWheelchair(boolean wheelchair) {
		this.wheelchair = wheelchair;
	}
	// mutator open 
	public void Setopen(boolean open) {
		this.open= open;
	}
	// string converter
	public String toString() { //returns one string of variables 
		return "name: " + this.name + "location: " + this.location + "wheelchair " + this.wheelchair + "open: " + this.open;
	}
	
	// requirement check
	public boolean checkLat() {
		if(this.getLat() > -90 && this.getLat() < 90) {
			return true;
		}else {
			return false;
		}
	}
	// requirement check 
	public boolean checkLng() {
		if(this.getLng()> -180 && this.getLng() < 180) {
			return true;
		}else {
			return false;
		}
	}
	// compares this instances to other GeoLocation
	public boolean equals(Object GeoLocation) {
		if(GeoLocation == null) {
			return false;
		}else {
			if(this == GeoLocation) {
				return true;
			}else {
				return false;
			}
		}
		
	}
}



