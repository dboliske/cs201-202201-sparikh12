package labs.lab1;

import java.util.Scanner;

public class Fahrenheit_To_Celsius {
    public static Scanner scan = new Scanner(System.in);
	public static void main(String[] args) {
		System.out.println("Program Name:Temperture Converstation");
		System.out.println("Name:SWAR PARIKH");
		System.out.println("Course:CS 201-02");
		System.out.println("Date:Jan 24 2022");
		// TODO Auto-generated method stub
		System.out.println("Enter the temperature in Fahrenheit");// input fahrenhiet 
		Double Fahrenheit = scan.nextDouble();
		System.out.println("Fahrenheit to Celsius");// outputs to celcius
		System.out.println((Fahrenheit-32)/1.8000);
		
		
		System.out.println("Enter the temperature in Celsius");//input celsius
		Double celsius = scan.nextDouble();
		System.out.println("Celsius to Fahrenheit");//outputs to fahrenheit
		System.out.println((celsius*1.8000) + 32); 
	}
// F-------->C
// input  expected   result
// 13F		-10.56C 	-10.555555555555555 C
// 16F		-8.89C		-8.88888888888889C
// 40F		4.444C	4.444444444444445C
// 70F 		21.1111C		21.11111111111111C	
//150F		65.5556C		65.55555555555556C
//250F		121.111C 	121.11111111111111C
// C-------->F
//input  expected  result
//-10C  	14F		14.0F
//-12C		10.4F	10.399999999999999F
//0C	    32F		32.0F
//2C		35.6F	35.6F
//40C 		104F	104.0F
//100C		212F	212.0F
// Yes the program ran according to the test plan of the programmed code.
}
