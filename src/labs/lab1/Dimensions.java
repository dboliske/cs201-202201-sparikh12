package labs.lab1;

import java.util.Scanner;

public class Dimensions {
	public static Scanner scan = new Scanner(System.in);
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Program Name:Dimensions");
		System.out.println("Name:SWAR PARIKH");
		System.out.println("Course:CS 201-02");
		System.out.println("Date:Jan 24 2022");
		System.out.println("Enter the Length");
		double length = scan.nextDouble();
		System.out.println("Enter the Width");
		double width = scan.nextDouble();
		System.out.println("Enter the Height");
		double height = scan.nextDouble();
		System.out.print("The square feet of wood needed is: ");
		System.out.println(2*length*width + 2*length*height + 2*width*height);
		//input  outcome 
		//5x5x5		150.0
		//4x5x6		148.0
		//9x4x6		228.0
	}

}
