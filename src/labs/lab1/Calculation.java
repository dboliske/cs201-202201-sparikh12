package labs.lab1;

import java.util.Scanner;

public class Calculation {
	
	public static Scanner scan = new Scanner(System.in);
	
	public static void main(String[] args) {
		System.out.println("Program Name:Calculation");
		System.out.println("Name:SWAR PARIKH");
		System.out.println("Course:CS 201-02");
		System.out.println("Date:Jan 24 2022");
		int my_age = 20; // my age
		int father_age = 49;// father age
		System.out.print("'The resultant age is: ");
		System.out.println(father_age-my_age);// father's age minus my age
		
		int birth_year = 2001;
		System.out.println(birth_year*2);// birth year times by 2
		
		double height_inches = 74;
		System.out.println();
		System.out.println(height_inches*2.54);// converting to cm 
		System.out.println(height_inches / 12);// convert to ft
		System.out.println();
		//double height_inch = scan.nextDouble();// to avoid extreme round off used double instead of int
		System.out.println(height_inches/12);// dividing by 12 to get ft
	}

}
