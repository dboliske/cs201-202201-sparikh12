# Lab 1

## Total

18/20

## Break Down

* Exercise 1    2/2
* Exercise 2    1.5/2
* Exercise 3    2/2
* Exercise 4
  * Program     2/2
  * Test Plan   1/1
* Exercise 5
  * Program     1.5/2
  * Test Plan   1/1
* Exercise 6
  * Program     2/2
  * Test Plan   0/1
* Documentation 5/5

## Comments
2. Only prints output of feet, not feet and inches
5. Have to convert from inches input to sq. feet output.
6. No test plan/table.