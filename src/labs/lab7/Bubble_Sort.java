package labs.lab7;

import java.util.Scanner;

public class Bubble_Sort {
	    
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// Bubble sort these numbers 
		System.out.println("Program Name:Sorting");
		System.out.println("Name:SWAR PARIKH");
		System.out.println("Course:CS 201-02");
		System.out.println("Date:April 04 2022");
		//```java
		//{10, 4, 7, 3, 8, 6, 1, 2, 5, 9}
		//```
		System.out.println("Bubble Sort");
		int[] Array = new int[] {10,4,7,3,8,6,1,2,5,9}; 
		boolean swapped = true; 
		
		do {
			swapped = false; 
			for ( int i = 1; i < Array.length; i ++){
				if (Array[i-1] > Array[i]){
					int bubble = Array[i-1];
					Array[i-1] = Array[i]; 
					Array[i] = bubble; 
					swapped = true; 
				}
			}
		}
		while (swapped); 
		int n1 = Array.length;
        for (int i=0; i<n1; ++i)
            System.out.print(Array[i] + " ");
        System.out.println();
// Insertion Sort
//```java
//{"cat", "fat", "dog", "apple", "bat", "egg"}
//```
        System.out.println("Insertion Sort");
        String[] insrt = {"cat", "fat", "dog", "apple", "bat", "egg"};
        int h = insrt.length;
        for (int i=1; i<h; ++i)
        {
            String key = insrt[i];
            int j = i-1;
  
            while (j>=0 && insrt[j].compareTo(key)>0)
            {
                insrt[j+1] = insrt[j];
                j = j-1;
            }
            insrt[j+1] = key;
        }
        for(int i = 0; i < h; i++)
        {
            System.out.println(i+": "+insrt[i]);
        }
        
// Selection Sort 
// ```java
// {3.142, 2.718, 1.414, 1.732, 1.202, 1.618, 0.577, 1.304, 2.685, 1.282}
//```
        System.out.println("Selection Sort");
        double[] num =  {3.142, 2.718, 1.414, 1.732, 1.202, 1.618, 0.577, 1.304, 2.685, 1.282};
        int n = num.length;
        for(int i = 0; i < n - 1; i++)
        {
         
            int min_index = i;
            double minStr = num[i];
            for(int j = i + 1; j < n; j++)
            {
            
             
                if(num[j]<(minStr))
                {
                   
                    minStr = num[j];
                    min_index = j;
                }
            }
     
	        if(min_index != i)
	        {
	            double temp = num[min_index];
	            num[min_index] = num[i];
	            num[i] = temp;
	        }
        }
        
        
        for(int i = 0; i < n; i++)
        {
            System.out.println(i+": "+num[i]);
        }
	

	


//Binary Search
//```java
//{"c", "html", "java", "python", "ruby", "scala"}
// ```
        System.out.println("Binary Search Sort");
	String[] arr = new String[] {"c","html","java","python","ruby","scala"}; 
	Scanner scan = new Scanner(System.in); 
	System.out.println("Enter only these words: \"c\",\"html\",\"java\",\"python\",\"ruby\",\"scala\"");
	System.out.println("Type here: ");
	String x = scan.nextLine();
	
	
	int l = 0, r = arr.length - 1;
    while (l <= r) {
        int m = l + (r - l) / 2;

        int a = x.compareTo(arr[m]);

        
        if (a == 0)
        	System.out.println(m);

        
        if (a > 0)
            l = m + 1;

        
        else
            r = m - 1;
    }

	
	
	
	
	
    }
	}

		
