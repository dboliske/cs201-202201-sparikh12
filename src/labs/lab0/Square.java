package labs.lab0;

public class Square {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Program Name:Square");
		System.out.println("Name:SWAR PARIKH");
		System.out.println("Course:CS 201-02");
		System.out.println("Date:Jan 15 2022");
		// pseudocode for hallow square 
		// making a 4x4 square 
		System.out.println("Hallow Square");
		// have top line by "-" x 4 ----> "- - - -"
		// right line by "|" x 4--------> for loop to print " -    -"
		// left line by "|" x 4----> above line will take care of left side as well
		//have bottom line by "-" x 4---> same as top so "- - - -" 
		System.out.println("- - - -");// prints the top line
		
		for(int i=1; i<=2; i++) { // this loop print the outer edge of square minus the inside part 
		System.out.println("-     -");// hence the loop runs twice only b/c 4 row - 2row= inside row of 2
		}
		System.out.println("- - - -");// prints the bottom line 
	
		//pseudocode for filled square 
		//making a 4x4 square
		// this can be done by for loop "- - - -" 
		//System.out.println("Filled Square");
		for(int i=1; i<=4; i++) {
			System.out.println("- - - -");
		}
}}
