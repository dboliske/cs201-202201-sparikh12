package labs.lab2;

import java.util.Scanner;

public class Menu {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Program Name:Menu");
		System.out.println("Name:SWAR PARIKH");
		System.out.println("Course:CS 201-02");
		System.out.println("Date:Jan 31 2022");
		Scanner input = new Scanner(System.in);
		//1.Say Hello - This should print "Hello" to console.
		//2.Addition - This should prompt the user to enter 2 numbers and return the sum of the two.
		//3.Multiplication - This should prompt the user to enter 2 numbers and return the product of the two.
		//4.Exit - Leave the program
		boolean status = false;
		while (true) {
			
			if (status){
				break;
			}
			System.out.println("1. Say Hello"); // This should print "Hello" to console.
			System.out.println("2. Addition " ); //- This should prompt the user to enter 2 numbers and return the sum of the two.");
			System.out.println("3. Multiplication");// - This should prompt the user to enter 2 numbers and return the product of the two.");
			System.out.println("4. Exit ");// - Leave the program");
			
			String in = input.next(); //scanning inputs 
			switch(in) {
				case "1": // case 1
					System.out.println("hello"); // prints hello
					break;
				case "2":
					System.out.println("Enter two numbers for Addition "); // reminds user two input two number
					int x = input.nextInt();// enter value 1
					int y= input.nextInt();// enter value 2
					int sum = (x+y); // adds value 1 and value 2
					System.out.println("Addition:   "+ sum); //displays back to user 
					
					break;
				case "3" :
					System.out.println("Enter two numbers for Multiplication");
					int a= input.nextInt();// enter 1
					int b= input.nextInt();// enter value 2
					int multi = (a*b);// multiplies value 1 and value 2
					System.out.println("Multiplication:     "+ multi);//displays back to user
				
					break;
					
				case "4":
					System.out.println("Program ended");// end the program if user inputs #4
					status = true;
					break;// cuts of the loop and wont display anything
				

			}
			
			
		
		}
		
		
		
	}

}
