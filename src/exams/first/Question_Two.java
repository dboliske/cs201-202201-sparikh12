package exams.first;

import java.util.Scanner;

public class Question_Two {
	public static Scanner scan = new Scanner(System.in);
	public static void main(String[] args) {
		System.out.println("Program Name:Question_Two");
		System.out.println("Name:SWAR PARIKH");
		System.out.println("Course:CS 201-02");
		System.out.println("Date:Feb 25-26 2022");
		System.out.println("     ");
		// TODO Auto-generated method stub
		//Write a program that prompts the user for an integer. If the integer is divisible by 2 print out "foo", and 
		//if the integer is divisible by 3 print out "bar". 
		//If the integer is divisible by both, your program should print out "foobar" and 
		//if the integer is not divisible by either, then your program should not print out anything.
		System.out.println("Enter an Integer Value");
		int value = scan.nextInt();
		if ((value%(2))== 0 && (value%(3))== 0 ) { // checks if the "value" is divisible by both 2 and 3
			System.out.println("Foobar");
			}
		else if ((value%(2))== 0 && (value%(3))!= 0) {// checks if the "value" is divisible by 2
			System.out.println("foo");
		}
		else if ((value%(2))!= 0 && (value%(3))== 0) {//checks if the "value" is divisible by 3
			System.out.println("bar");
		}
		else if ((value%(2))!= 0 && (value%(3))!= 0) {//checks if the "value" is not divisible by both 2 and 3
		
		}
		
		
	}

}
