package exams.first;

import java.util.Scanner;
public class Question_One {
	public static Scanner scan = new Scanner(System.in);
	public static void main(String[] args) {
		System.out.println("Program Name:Question_One");
		System.out.println("Name:SWAR PARIKH");
		System.out.println("Course:CS 201-02");
		System.out.println("Date:Feb 25-26 2022");
		System.out.println("     ");
		// TODO Auto-generated method stub
		//Write a program that prompts the user for an integer, add 65 to it, convert the result to a character and print that character to the console.
		System.out.println("Enter an the integer");// prompts the user for an integer
		int len= scan.nextInt();	// allows and reads the user inputs
		char a= (char)(len+65);// converts the int to character & adding 65
		//System.out.println(len);
		System.out.println(a);// prints to the console
		
	}

}
