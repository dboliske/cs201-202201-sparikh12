package exams.first;
import java.lang.Thread;
import java.io.*;

import java.util.Scanner;

public class Question_Three {
	public static Scanner scan = new Scanner(System.in);
	public static void main(String[] args) throws InterruptedException {
		System.out.println("Program Name:Question_Three");
		System.out.println("Name:SWAR PARIKH");
		System.out.println("Course:CS 201-02");
		System.out.println("Date:Feb 25-26 2022");
		System.out.println("     ");
		// TODO Auto-generated method stub
		//Write a program that prompts the user for an integer and then prints out a Triangle of that height and width. 
		//For example, if the user enters 3, then your program should print the following:
		System.out.println("Enter the Number for Triangle print");
		int star = scan.nextInt();
		String txt = "";
		String space = " ";
		String Star = "*";
		
		String str1  = ""; 
		String str2 = "";
		
		for (int i=0;i<=star; i++) { // x row
			
				
				for(int x = 0;x <i;x++) {// using i 
					str1 = str1 + space; // creates spaces 
				}
				for(int x = 0;x <(star-i);x++) { // using i
					str2 = str2 + Star;// creates stars
				}
				txt = str1 + str2; // combines the strings 
				
			
			
			
			System.out.println(txt);
			 txt = "";// emptying the strings
			 str1  = "";// emptying the strings
			 str2 = "";// emptying the strings
		}
	}

}
