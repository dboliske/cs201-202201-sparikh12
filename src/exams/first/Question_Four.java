package exams.first;

import java.util.Scanner;

public class Question_Four {
	public static Scanner scan = new Scanner(System.in);
	public static void main(String[] args) {
		System.out.println("Program Name:Question_Four");
		System.out.println("Name:SWAR PARIKH");
		System.out.println("Course:CS 201-02");
		System.out.println("Date:Feb 25-26 2022");
		System.out.println("     ");
		// TODO Auto-generated method stub
//		Write a program that prompts the user for 5 words and prints out any word that appears more than once. 
//
//		NOTE: 
//		�The words should be an exact match, i.e. it should be case sensitive.
//		�Your programustuse an array.
//		�Do not sort the data or use an ArrayLists.
		System.out.println("Enter the Five words");
		String[] Array = new String[5];
		
		for(int i=0;i<5;i++) {
			System.out.print("Enter the word:"); // prompts user for the five words
			String txt = scan.nextLine();
			Array[i] = txt;
		}
		
		boolean status = false;
		
		for(int i=0;i<5;i++) { //checks the word against other words
			for(int j=i;j<5;j++) {
				if(Array[i].equals(Array[j]) && i != j) {// avoids the second word match by setting it false.(accepts first match)
					System.out.print(Array[i]);
					status = true;
				}
			}
		}
		if (!status) {
			System.out.print("No similar words found!");
		}
		
		
		
		

	}}
