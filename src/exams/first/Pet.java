package exams.first;

public class Pet {
	private String name;
	private int age;
	
	// default const
	public Pet() {
		this.name= "";
		this.age=0;
	}
	// non default const
	public Pet(String name, int age) {
		this.name = name;
		
		if(this.age > 0 )
			this.age = age;
		else {
			System.out.println("ERROR: The age is incorrect so the value is set to 0!");
			this.age = 0;
		}
	}
	// name access
	public String getName() {
		return this.name;
	}
	// age access
	public int getAge() {
		return this.age;
	}
	// mutator name
	public void setName(String name) {
		this.name = name;
	}
	// mutator age
	public void setAge(int age) {
		
		
		if(this.age > 0 )
			this.age = age;
		else {
			System.out.println("ERROR: The age is incorrect so the value is unchaged!");
			this.age = this.age;
		}
	}
	// string converter
	public String toString() {
		String txt = "Pet Name : " + this.name +" Pet Age: " + this.age;
		return txt;
	}
	//comparison for the instances to the other Pet
	public boolean equals(Object Pet) {
		if(Pet == null) {
			return false;
		}else {
			if(this == Pet) {
				return true;
			}else{
				return false;
			}
		}
	}
	
	
}
