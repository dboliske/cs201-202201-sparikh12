package exams.second;

public class Computer_Lab {
	private boolean computers;
	
	//default
	public Computer_Lab() {
		this.computers = true;
	}
	//non-default
	public Computer_Lab(boolean computers) {
		this.computers = computers;
	}
	//computer access
	public boolean getComputers() {
		return this.computers;
	}
	//mutator computer
	public boolean Computers(boolean computers) {
		return this.computers;
	}
	public String toString() {
		String txt = "computers : " + this.computers;
		return txt;
	}
	
}
