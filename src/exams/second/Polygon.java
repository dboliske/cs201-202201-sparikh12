package exams.second;

public class Polygon extends Rectangle {
	private static final String Name = null;
	private String name;
	
	//default const
	public Polygon() {
		this.name = "";
	}
	// non default const
	public Polygon(String name) {
		this.name = name;
		
	}
	// Name Access
	public String getName() {
		return this.name;
	}
	//mutator name
	public void setname(String name) {
		this.name= name;
	}
	public String toString() {
		String txt = "Polygon :" + this.Name;
		return txt;
	}
	//area of Rectangle
	public double area(Rectangle Area){
			double area = Area.getHeight() * Area.getWidth(); 
			return area; 
		}
	//perimeter of rectangle
	public double perimeter(Rectangle Per){
			double perimeter = 2*(Per.getHeight() + Per.getWidth()); 
			return perimeter;
			}}
//	//area of circle
//	public double area(Circle Area){
//				double area = Math.PI*getRadius()*getRadius();  
//				return area; 
//	}
//	//perimeter of circle
//	public double perimeter(Circle Per){
//				double perimeter = 2*Math.PI*getRadius(); 
//				return perimeter; 
//			}
//}
