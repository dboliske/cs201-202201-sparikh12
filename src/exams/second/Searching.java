package exams.second;

import java.util.Scanner;

public class Searching {

	public static int jumpSearch(double[] arr, double x)
    {
        int n = arr.length;
 
        // Finding block size to be jumped
        int step = (int)Math.floor(Math.sqrt(n));
 
        int old = 0;
        while (arr[Math.min(step, n)-1] < x)
        {
            old = step;
            step += (int)Math.floor(Math.sqrt(n));
            if (old >= n)
                return -1;
        }
 
        
        while (arr[old] < x)
        {
            old++;
 
            if (old == Math.min(step, n))
                return -1;
        }
 
        // If element is found
        if (arr[old] == x)
            return old;
 
        return -1;
    }
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Enter a number from {0.577, 1.202, 1.282, 1.304, 1.414, 1.618, 1.732, 2.685, 2.718, 3.142}");
		Scanner scan = new Scanner(System.in); 
		String x = scan.nextLine();
		double arr[] = {0.577, 1.202, 1.282, 1.304, 1.414, 1.618, 1.732, 2.685, 2.718, 3.142};
		double s = Double.parseDouble(x);

    // Find the index of 'x' using Jump Search
    double index = jumpSearch(arr, s);

    // Print the index where 'x' is located
    System.out.println("\nNumber " + s +
                        " is at index " + index);
}
	}
	