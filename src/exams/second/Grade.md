# Final Exam

## Total

64/100

## Break Down

1. Inheritance/Polymorphism:    11/20
    - Superclass:               5/5
    - Subclass:                 0/5
    - Variables:                3/5
    - Methods:                  3/5
2. Abstract Classes:            8/20
    - Superclass:               5/5
    - Subclasses:               0/5
    - Variables:                2/5
    - Methods:                  1/5
3. ArrayLists:                  5/20
    - Compiles:                 0/5
    - ArrayList:                5/5
    - Exits:                    0/5
    - Results:                  0/5
4. Sorting Algorithms:          20/20
    - Compiles:                 5/5
    - Selection Sort:           10/10
    - Results:                  5/5
5. Searching Algorithms:        20/20
    - Compiles:                 5/5
    - Jump Search:              10/10
    - Results:                  5/5

## Comments

1. Vars are protected, not private. Allows for non-positive values. Inheritance is done in the wrong direction from the UML diagram.
2. No inheritance at all. "The radius, height, and width should always be positive and can default to 1." Polygon var should be protected, not private. 
3. Program doesn't work but it uses ArrayList correctly
4.
5.
