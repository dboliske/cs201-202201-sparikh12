package exams.second;

public class Rectangle {
	private double height;
	private double width;
	
	// default Const 
	public Rectangle(){
		this.height = 0.00 ;
		this.width = 0.00;
	}	
	//Non-default Const
	public Rectangle(double height, double width){ 
		this.height = height;
		this.width = width;
	}
	// Height Access
	public double getHeight(){
		return this.height; 
	}
	// mutator height
	public void setheight(double height){
		this.height = height; 
	}
	// Width Access
	public double getWidth(){
		return this.width; 
	}
	// mutator width
	public void setwidth(double width){
		this.width = width; 
	}
	
	public String toString(){
		 String txt = "Rectangle "+ "\n" + "Height: " + this.height + "\nWidth: " + this.width ; 
		return txt;
	}
	//area of rectangle
	public double area(Rectangle Area){
		double area = Area.getHeight() * Area.getWidth(); 
		return area; 
	}
	//perimeter of rectangle
	public double perimeter(Rectangle Per){
		double perimeter = 2*(Per.getHeight() + Per.getWidth()); 
		return perimeter; 
	}
	public boolean equals(Object Rectangle){
		if (Rectangle == null) {
			return false;
		}else {
			if(this == Rectangle) {
				return true;
			}
		else {
			return false;
			}
		}
	}
}
