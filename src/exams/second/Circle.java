package exams.second;

public class Circle {
	private double radius;
	
	
	// default Const 
	public Circle(){
		this.radius = 0.00 ;
		
	}	
	//Non-default Const
	public Circle (double radius){ 
		this.radius = radius;
		
	}
	// radius Access
	public double getRadius(){
		return this.radius; 
	}
	// mutator radius
	public void setradius(double radius){
		this.radius = radius; 
	}
	
	
	public String toString(){
		 String txt = "circle "+ "\n" + "radius: " + this.radius ; 
		return txt;
	}
	//area of circle
	public double area(Circle Area){
		double area = Math.PI*getRadius()*getRadius();  
		return area; 
	}
	//perimeter of circle
	public double perimeter(Circle Per){
		double perimeter = 2*Math.PI*getRadius(); 
		return perimeter; 
	}
	public boolean equals(Object Circle){
		if (Circle == null) {
			return false;
		}else {
			if(this == Circle) {
				return true;
			}
		else {
			return false;
			}
		}
	}
}

