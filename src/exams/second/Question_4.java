package exams.second;



public class Question_4 {

	public static String[] sort(String[] array) {
		for (int i=0; i<array.length - 1; i++) {
			int min = i;
			for (int j=i+1; j<array.length; j++) {
				if (array[j].compareTo(array[min]) < 0) {
					min = j;
				}
			}
			
			if (min != i) {
				String temp = array[i];
				array[i] = array[min];
				array[min] = temp;
			}
		}
		
		return array;
	}


	public static void main(String[] args) {
		// TODO Auto-generated method stub
		/*
		Write a Java program that implements the Selection Sort Algorithm for an Array (or ArrayList) of Strings and prints the sorted results.

		```java
		{"speaker", "poem", "passenger", "tale", "reflection", "leader", "quality", "percentage", "height", "wealth", "resource", "lake", "importance"}
		```
		*/
		System.out.println("Selection Sort");
		String[] a = new String[]{"speaker", "poem", "passenger", "tale", "reflection", "leader", "quality", "percentage", "height", "wealth", "resource", "lake", "importance"};
		a= sort(a);
		for (String s : a) {
			System.out.print(s + " ");
		}
	}

}
