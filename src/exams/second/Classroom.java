package exams.second;

public class Classroom extends Computer_Lab{
	  private String building;
	  private String roomNumber;
	  private int seats;
	

	// Default const
	public Classroom(){
	  this.building = "";
	  this.roomNumber = "";
	  this.seats = 0 ;
	  
	}
	// Non-Default const
	public Classroom (String buidling, String roomNumber, int seats){
	  this.building= buidling;
	  this.roomNumber= roomNumber;
	  this.seats = seats;
	}
	//bulding access
	public String  getBuilding(){
	  return this.building;
	}
	//Name access
	public String getRoomNumber(){
	  return this.roomNumber;
	}
	
	//Seats access
	public int getSeats(){
	  return this.seats;
	}
	// mutator bulding
	public void setBuilding(String building) {
		this.building= building;
	}
	//mutator roomNumber
	public void setRoomNumber(String roomNumber) {
		this.roomNumber= roomNumber;
	}
	//mutator seats
	public void setSeats(int seats) {
			this.seats= seats;
	}
	public String toString() {
		String txt = "Classroom :" + this.building+"  RoomNumber:"+this.roomNumber+ "  seats:"+this.seats;
		return txt;
	}
		

public boolean equals(Object item ) { 
	if (item == null) {
		return false;
	}else {
		if(this == item) {
			return true;
		}
	else {
		return false;
		}
	}
  }
}

		
