package project;

import java.io.File;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import java.io.FileWriter;   // Import the FileWriter class
import java.io.IOException;  // Import the IOException class to handle errors


public class StoreApp  {
	

	public static void main(String[] args) throws IOException{
		// TODO Auto-generated method stub
		Store newStore = new Store();
		System.out.println("Program Name:Final Store Project");
		System.out.println("Name:SWAR PARIKH");
		System.out.println("Course:CS 201-02");
		System.out.println("Date:April 29 2022");
		// tests
		newStore.setId("1234567890");
		System.out.println(newStore.getId());
		newStore.setName("Test Name");
		System.out.println(newStore.getName());
		
		System.out.print("Adding new item to inventory");
		newStore.addNewItem("14356543", "This is test", 10, "None", 100000000);
		System.out.println("Printing the new inventory");
		for(int i = 0;i<newStore.getInventory().size();i++) {
			System.out.println(newStore.getInventory().get(i));
		}
		
	
	//read input file
		
		 
	File file= new File("C:/Users/Swarp/git/cs201-202201-sparikh12/src/project/stock.csv");
	Scanner input = new Scanner(file);
	
		 
		 ArrayList<String> x = new ArrayList<String>();
		 while (input.hasNextLine()) {
		        String data = input.nextLine();
		        x.add(data);
		      }
		int count = 0;
		int id = 0;
		 for(int i = 0;i<x.size();i++) {
			 
			 if(i == x.size()-1) {
				 break;
			 }
			 
			 if(x.get(i).compareTo(x.get(i+1)) != 0) {
				 newStore.addNewItem(Integer.toString(id), x.get(i).split(",")[0], Float.parseFloat(x.get(i).split(",")[1]), x.get(i).split(",")[0], count);
				 id++;
				 count = 0;
				 
			 }else {
				 count ++;
			 }
		 }
		 
		
		 
		
		
				 
		
		Scanner scan = new Scanner(System.in);			 
		
				 
		boolean isTrue = true;		 
		while(isTrue) {
			
			System.out.println("Welcome to Swar's swaggy life");
			System.out.println("Please choose an option from the menu displayed");
			System.out.println("1: Add new Item");
			System.out.println("2: Sell item");
			System.out.println("3: Search item ");
			System.out.println("4: Modify item");
			System.out.println("5: exit");
			
			int user_option = scan.nextInt();
			
			switch(user_option) {
			case 1:
				System.out.println("Enter Item ID:");
				String ID = scan.next();
				System.out.println("Enter Item name:");
				String item_name = scan.next();
				System.out.println("Enter Item price:");
				float item_price = scan.nextFloat();
				System.out.println("Enter Item description:");
				String item_description = scan.next();
				System.out.println("Enter quantity:");
				int item_quantity = scan.nextInt();
				newStore.addNewItem(ID, item_name , item_price, item_description, item_quantity);
				
				try {
					File myObj = new File("StoreCount.csv");
				      if (myObj.createNewFile()) {
				        System.out.println("File created: " + myObj.getName());
				      } else {
				        System.out.println("File already exists.");
				      }
				
				      FileWriter myWriter = new FileWriter("StoreCount.csv");
				      for(int i = 0;i<newStore.getInventory().size();i++) {
				    	  myWriter.write(newStore.getInventory().get(i).getId()+","+newStore.getInventory().get(i).getName()+","+newStore.getInventory().get(i).getPrice()+","+newStore.getInventory().get(i).getQuantity()+"\n");
				      }
				     
				      myWriter.close();
				      System.out.println("Successfully wrote to the file.");
				    } catch (IOException e) {
				      System.out.println("An error occurred.");
				      e.printStackTrace();
				    }
				break;
			
			
			case 2:
				//make a cart of items
				
				
				
				break;
				
			case 3:
				System.out.println("Please enter item name");
				String name = scan.next();
				
				
				System.out.print(newStore.search(name));
				// store item name
				//search by calling search method with item name
				break;
				
			case 4:
				System.out.println("Enter p to update price and enter n to update name");
				//store letter
				// if p--> setPrice
				// if n --> setName
				
				break;
				
			case 5:
				
				System.out.println("Have a good day!");
				isTrue = false;
				break;
				
			default:
				System.out.println("invalid option");
			
		}
		}
		 
	}

}
