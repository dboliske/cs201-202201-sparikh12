# Final Project

## Total

92/150

## Break Down

Phase 1:                                                            40/50

- Description of the user interface                                 3/3
- Description of the programmer's tasks:
  - Describe how you will read the input                            3/3
  - Describe how you will process the data from the input file      4/4
  - Describe how you will store the data                            3/3
  - How will you add/delete/modify data?                            3/5
  - How will you search data?                                       4/5
- Classes: List of names and descriptions                           7/7
- UML Class Diagrams                                                10/10
- Testing Plan                                                      3/10

Phase 2:                                                            52/100

- Compiles and runs with no run-time errors                         3/10
- Documentation                                                     3/15
- Test plan                                                         0/10
- Inheritance relationship                                          3/5
- Association relationship                                          0/5
- Searching works                                                   5/5
- Uses a list                                                       5/5
- Project reads data from a file                                    5/5
- Project writes data to a file                                     5/5
- Project adds, deletes, and modifies data stored in list           3/5
- Project generates paths between any two stations                  10/10
- Project encapsulates data                                         5/5
- Project is well coded with good design                            2/10
- All classes are complete (getters, setters, toString, equals...)  5/5

## Comments
Does not include how items will be added, and a different method should be used for remove and modify.
While you can search for items sequentially, the run time of the code will go down using binary search (O(n) vs O(log(n))). This can matter as the amount of items in a general store can be large.
Missing descriptions for classes but they are intuitive.
The test plan should cover a number of specific things or types of things (valid/invalid input) to input into the scanner in order to give determinate results (success, try again, etc.). This can easy to test using a csv file and scanning it as input.
### Design Comments

### Code Comments
Not entering an integer when expected will make code exit with error for a lot of scanner options, lots of code missing.
There is no documentation at the top of files, as well as very few descriptors for what the code actaully does.
There is no test plan file to test.
There is no sell item implemented, search item is linear, modify item doesn't work, and not giving the correct input tends to make the program exit with an error. Overall, a lot more effort can be put into improving the functionality and testing of this project.