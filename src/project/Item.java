package project;

public class Item extends Store {
	  private String id;
	  private String name;
	  private float price;
	  private String description;
	  private int quantity;
	

	// Default const
	public Item(){
	  this.id = "";
	  this.name = "";
	  this.price = 0.0f;
	  this.description = "";
	  this.quantity = 0 ;
	  
	}
	// Non-Default const
	public Item (String id, String name, String description,float price,int quantity){
	  this.id= id;
	  this.name= name;
	  this.description= description;
	  this.price= price;
	  this.quantity = quantity;
	}
	//ID access
	public String  getId(){
	  return this.id;
	}
	//Name access
	public String getName(){
	  return this.name;
	}
	//Description access
	public String getDescription(){
	  return this.description;
	}
	//price access
	public float getPrice(){
	  return this.price;
	}
	//quantity access
	public int getQuantity(){
	  return this.quantity;
	}
	// mutator id
	public void setId(String id) {
		this.id= id;
	}
	//mutator name
	public void setName(String name) {
		this.name= name;
	}
	//mutator Description
	public void setDescription(String description) {
		this.description= description;
	}
	//mutator price
	public void setPrice(float price) {
		this.price= price;
	}
	//mutator quantity
	public void setqQuantity(int quantity) {
			this.quantity= quantity;
	}
	public String toString() {
		String txt = "Product :" + this.id+"  Product name:"+this.name + "  Description"+ this.description +"  Quatity:"+this.quantity+"  Price:"+this.price;
		return txt;
	}
		

public boolean equals(Object item ) { 
	if (item == null) {
		return false;
	}else {
		if(this == item) {
			return true;
		}
	else {
		return false;
		}
	}
  }
}

	  

