package project;

import java.util.ArrayList;


public class Store {
	private String id;
	private String name;
	//private Item Cart[];
	//private Item Inventory[];
	ArrayList<Item> Inventory = new ArrayList<Item>();
	ArrayList<Item> Cart = new ArrayList<Item>();

	// Default const
	public Store(){
		this.id = "";
		this.name = "";
		
		
	}
	// Non-Default const
	public Store(String id, String name){
		this.id= id;
		this.name= name;
	}
	//ID access
	public String  getId(){
		return this.id;
	}
	//Name access
	public String getName(){
		return this.name;
	}
	//get inventory
		public ArrayList<Item> getInventory(){
			return this.Inventory;
		}
	// mutator id
	public void setId(String id) {
		this.id= id;
	}
	//mutator name
	public void setName(String name) {
		this.name= name;
	}
	
	public String addInventoryItem(Item i)
	{
		String item_id = "";
		Inventory.add(i);
		item_id = i.getId();
		
		return item_id;
		
	}
	
	public float sellItem(ArrayList<Item> cart) {
		
		float total_price = 0;
		for(int i = 0 ; i < cart.size(); i++)
		{
			Inventory.remove(cart.get(i));
			total_price+= cart.get(i).getPrice();
		}
		
		return total_price;
		
	}
	
	public String duplicateItem(Item item) {//duplication 
		Item newItem = new Item(item.getId(), item.getName(), item.getDescription(), item.getPrice() , item.getQuantity());
		
		Inventory.add(newItem);
		return newItem.getId();
		
	}
	
	
	public void addNewItem(String itemID , String ItemName, float ItemPrice , String Itemdescription, int itemQuantity)
	{
		
		Item newItem = new Item(itemID, ItemName, Itemdescription, ItemPrice , itemQuantity);
		Inventory.add(newItem);
	}
	
	public String search(String itemName) {
		
		String name = "";
		for(int i = 0 ; i < Inventory.size() ; i++) {
			if(Inventory.get(i).getName().equals(itemName))
			{
				name =   ("Item Name: "+ Inventory.get(i).getName()+"  Item ID: "+ Inventory.get(i).getId()+"  Price: "+ Inventory.get(i).getPrice()+"  Quantity: "+ Inventory.get(i).getQuantity()+"/n"); 
			}
		}
		
		return  name;	
		
	}
	
	
	public void removeItem(String itemName)
	{
		for(int i = 0 ; i < Inventory.size() ; i++) {
			if(Inventory.get(i).getName().equals(itemName))
			{
				Inventory.remove(i);
			}
		}
	}
	
	public String toString()
	{
		return ("Store ID:"+this.id+"  Store Name:"+this.name);
		
	}
	
	
	
}